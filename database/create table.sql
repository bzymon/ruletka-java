CREATE TABLE uzytkownik(
    id NUMBER(5) CONSTRAINT uzytkownik_pk PRIMARY KEY,
    username VARCHAR2(20) NOT NULL UNIQUE,
    password VARCHAR2(30) NOT NULL,
    money NUMBER(10) NOT NULL
);

CREATE TABLE transakcja(
	id NUMBER(5) CONSTRAINT transakcja_pk PRIMARY KEY,
	data DATE NOT NULL,
	kwota NUMBER(10) NOT NULL,
	forma_platnosci VARCHAR2(20) NOT NULL,
    uzytkownik_username VARCHAR2(20) CONSTRAINT transakcja_fk REFERENCES uzytkownik(username) NOT NULL
);

CREATE SEQUENCE transakcja_id_seq START WITH 10;
CREATE SEQUENCE uzytkownik_id_seq START WITH 10;