package sample;

import javafx.fxml.FXML;
import javafx.scene.control.*;
import java.io.*;
import java.net.*;
import java.util.Random;

import javafx.stage.Stage;

public class ServerRouletteController {
    Random random = new Random();
    private ServerSocket serversocket = null;
    private Socket socket = null;
    private PrintWriter printWriter = null;
    private InputStreamReader in = null;
    private BufferedReader bf = null;

    @FXML
    private Button button;

    @FXML
    public TextField amountOfRandomNumbersText;

    public ServerRouletteController(){
        try {
            serversocket = new ServerSocket(4999);
            System.out.println("Serwer wystartowal");
            System.out.println("Oczekiwanie na polaczenie sie z klientem...");
            socket = serversocket.accept();
            System.out.println("Klient polaczyl sie z serwerem.");
            serversocket.setSoTimeout(9999);    //by polaczenie nie zostalo automatycznie zerwane.
            printWriter = new PrintWriter(socket.getOutputStream());
            in = new InputStreamReader(socket.getInputStream());
            bf = new BufferedReader(in);
        }
        catch(IOException e) {
            e.printStackTrace();
        }
    }

    public void handleButtonEvent() {
        String val = amountOfRandomNumbersText.getText();
        if(!val.matches("\\d+")) {
            val = "0";
            amountOfRandomNumbersText.setText(val);
        }
        int amountOfRandomNumbersInt = Integer.parseInt(val);
        if(amountOfRandomNumbersInt > 1000) {
            amountOfRandomNumbersInt = 1000;
            amountOfRandomNumbersText.setText("1000");
        }
        else if(amountOfRandomNumbersInt < 0) {
            amountOfRandomNumbersInt = 0;
            amountOfRandomNumbersText.setText("0");
        }

        System.out.println("Liczba wygenerowanych liczb: " + amountOfRandomNumbersInt);

        for(int i=0; i<amountOfRandomNumbersInt; i++) {
            int answer_number = random.nextInt(37);
            printWriter.println(answer_number);
            printWriter.flush();
        }
        if(amountOfRandomNumbersInt != 0) {
            button.setDisable(true);
            button.setText("Serwer wygenerowal liczby");
        }

        printWriter.println("99");  //znak konca wygenerowanych liczb
        printWriter.flush();
    }

    public void serverClose() throws IOException {
        serversocket.close();
        System.out.println("Serwer zostal wyłączony.");
        Stage stage = (Stage) button.getScene().getWindow();
        stage.close();
        printWriter.close();
        bf.close();
    }
}
