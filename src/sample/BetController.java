package sample;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class BetController {
    private String betSelectedColor;
    private int betMoney = 0;
    private String betMoneyInString;
    private int betSelectedNumber;

    @FXML
    public TextField txtBetDetails;

    @FXML
    public void handleCloseEvent(javafx.event.ActionEvent actionEvent) {
        ((Stage)(((Button)actionEvent.getSource()).getScene().getWindow())).hide();
    }

    public String convertMoneyToString() {
        betMoneyInString = Integer.toString(betMoney);
        return betMoneyInString;
    }

    @FXML
    public int add100ToBetMoney() {
        betMoney += 100;
        convertMoneyToString();
        txtBetDetails.setText("Postawiono: " + betMoneyInString);
        return betMoney;
    }

    @FXML
    public int add50ToBetMoney() {
        betMoney += 50;
        convertMoneyToString();
        txtBetDetails.setText("Postawiono: " + betMoneyInString);
        return betMoney;
    }

    @FXML
    public int getButtonId(javafx.event.ActionEvent actionEvent) {
        Button btn = (Button) actionEvent.getSource();
        String id = btn.getId();
        int btn_id = Integer.parseInt(id.substring(id.length() - 2));
        return btn_id;
    }

    @FXML
    public void pressBetREDButton(javafx.event.ActionEvent actionEvent) {
        betSelectedColor = "CZERWONY";
        betSelectedNumber = -1;
        getBetMoney();
        handleCloseEvent(actionEvent);
    }

    @FXML
    public void pressBetBLACKButton(javafx.event.ActionEvent actionEvent) {
        betSelectedColor = "CZARNY";
        betSelectedNumber = -1;
        getBetMoney();
        handleCloseEvent(actionEvent);
    }

    @FXML
    public void pressBetGREENButton(javafx.event.ActionEvent actionEvent) {
        betSelectedColor = "ZIELONY";
        betSelectedNumber = 0;
        getBetMoney();
        handleCloseEvent(actionEvent);
    }

    @FXML
    public void pressBetNumberREDButton(javafx.event.ActionEvent actionEvent) {
        betSelectedColor = "CZERWONY";
        betSelectedNumber = getButtonId(actionEvent);
        getBetMoney();
        handleCloseEvent(actionEvent);
    }

    @FXML
    public void pressBetNumberBLACKButton(javafx.event.ActionEvent actionEvent) {
        betSelectedColor = "CZARNY";
        betSelectedNumber = getButtonId(actionEvent);
        getBetMoney();
        handleCloseEvent(actionEvent);
    }

    public String getBetSelectedColor() {
        return betSelectedColor;
    }

    public int getBetMoney() {
        return betMoney;
    }

    public int getBetSelectedNumber() {
        return betSelectedNumber;
    }
}