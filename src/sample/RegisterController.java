package sample;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javafx.stage.Window;

import java.io.IOException;
import java.sql.*;

public class RegisterController {

    @FXML
    public TextField txtRegUsername;

    @FXML
    public TextField txtRegPassword;

    @FXML
    public TextField txtRegConfirm;


    @FXML
    public void handleCloseEvent(javafx.event.ActionEvent actionEvent) {
        ((Stage)(((Button)actionEvent.getSource()).getScene().getWindow())).hide();
    }

    FXMLLoader loader = new FXMLLoader(getClass().getResource("client.fxml"));
    Parent root = loader.load();
    ClientController clientController =
            loader.<ClientController>getController();

    public RegisterController() throws IOException {
    }

    @FXML
    public void pressConfirmRegisterButton() throws IOException {
        Connection conn1 = null;
        String typedUsername = txtRegUsername.getText();
        String typedPassword = txtRegPassword.getText();
        String typedConfirmation = txtRegConfirm.getText();

        if (typedUsername.length()>4 && typedPassword.length()>4 && typedConfirmation.length()>4) {
            if (typedPassword.equals(typedConfirmation)) {
                try {
                    Class.forName("oracle.jdbc.OracleDriver");
                    String dbURL1 = "jdbc:oracle:thin:" + clientController.DB_USERNAME + "/" + clientController.DB_PASSWORD + "@localhost:1521:xe";
                    conn1 = DriverManager.getConnection(dbURL1);
                    if (conn1 != null) {
                        Statement stmt = conn1.createStatement();
                        Statement stmt2 = conn1.createStatement();
                        ResultSet checker = stmt2.executeQuery("SELECT username FROM uzytkownik");
                        while (checker.next())
                            if (checker.getString(1).equals(typedUsername)) {
                                Alert usernameDoubled = new Alert(Alert.AlertType.ERROR);
                                usernameDoubled.setTitle("Rejestracja nieudana");
                                usernameDoubled.setHeaderText("Nazwa uzytkownika jest zajeta!");
                                usernameDoubled.showAndWait();
                                return;
                            }

                        ResultSet rs = stmt.executeQuery("INSERT INTO uzytkownik VALUES (uzytkownik_id_seq.NEXTVAL," + "'" + typedUsername + "', '" + typedPassword + "' , 5000)");
                    }
                    if (conn1 != null && !conn1.isClosed()) {
                        conn1.close();
                    }
                } catch (ClassNotFoundException ex) {
                    ex.printStackTrace();
                } catch (
                        SQLException ex) {
                    ex.printStackTrace();
                }
                Alert registrationSuccessful = new Alert(Alert.AlertType.INFORMATION);
                registrationSuccessful.setTitle("Rejestracja zakonczona powodzeniem");
                registrationSuccessful.setHeaderText("Mozesz teraz zalogowac sie, korzystajac z wprowadzonych danych.");
                registrationSuccessful.showAndWait();
                Window stage = txtRegConfirm.getScene().getWindow();
                stage.hide();

            } else {
                Alert passwordsNotEqual = new Alert(Alert.AlertType.ERROR);
                passwordsNotEqual.setTitle("Rejestracja nieudana");
                passwordsNotEqual.setHeaderText("Hasla nie sa takie same!");
                passwordsNotEqual.showAndWait();
            }
        }
        else {
            Alert requirementsNotMet = new Alert(Alert.AlertType.ERROR);
            requirementsNotMet.setTitle("Rejestracja nieudana");
            requirementsNotMet.setHeaderText("Zarowno nazwa uzytkownika, jak i haslo musza miec minimum 5 znakow!");
            requirementsNotMet.showAndWait();
        }
    }
}
