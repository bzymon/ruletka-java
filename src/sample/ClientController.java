package sample;

import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

import java.io.IOException;
import java.sql.*;
import java.util.Optional;
import java.io.*;
import java.net.*;

import static java.sql.Types.NULL;

public class ClientController {

    // LOKALNA BAZA DANYCH - DANE DO LOGOWANIA
    final public String DB_USERNAME = "baza";
    final public String DB_PASSWORD = "baza";
    //

    private Integer[] isRed = {1,3,5,7,9,12,14,16,18,19,21,23,25,27,30,32,34,36};
    private Integer[] isBlack = {2,4,6,8,10,11,13,15,17,20,22,24,26,28,29,31,33,35};
    public String color = null;
    private int money = 1000;
    public int betMoney;
    public int betNumber;
    Connection conn1 = null;
    public String currentUser;

    //Zmienne wykorzystywane do utworzenia polaczenia z serwerem
    Socket socket = null;
    PrintWriter printWriter = null;
    InputStreamReader in = null;
    BufferedReader bf = null;

    @FXML
    public Button btnRoll;

    @FXML
    public TextField txtResult;

    @FXML
    private TextField txtMon;

    @FXML
    private TextArea txtBetType;

    @FXML
    private TextField txtUsername;

    @FXML
    private PasswordField txtPassword;

    @FXML
    public Button btnLogout;

    @FXML
    public Button btnLogin;

    @FXML
    public Text txtLoggedIn;

    @FXML
    public Button btnRegister;

    @FXML
    public Button btnAccount;

    //konstruktor okna klienta podlacza sie do serwera.
    public ClientController() {
        try {
            socket = new Socket("localhost", 4999);
            System.out.println("Podlaczam sie do serwera.");
            printWriter = new PrintWriter(socket.getOutputStream());
            in = new InputStreamReader(socket.getInputStream());
            bf = new BufferedReader(in);
        }
        catch(IOException i) {
            System.out.println(i);
            Alert serverIsOff = new Alert(Alert.AlertType.WARNING);

            serverIsOff.setTitle("Serwer jest wyłączony!");
            serverIsOff.setHeaderText("Aby uruchomić grę, należy najpierw włączyć serwer.");

            ButtonType confirmExit = new ButtonType("Koniec");
            serverIsOff.getButtonTypes().setAll(confirmExit);
            serverIsOff.showAndWait();

            Platform.exit();
            System.exit(0);
        }
    }

    //Przyrównuje wylosowaną wartość do danej tablicy.
    private boolean compareInt_Arrays(int singleInt, Integer[] intArray) {
        boolean val = false;
        for (int i = 0; i < intArray.length; i++) {
            if (singleInt == intArray[i]) {
                val = true;
                break;
            }
            else
                val = false;
        }
        return val;
    }

    //Metoda wywoływana przy porażce
    public void defeat()  {
        Alert porazka = new Alert(Alert.AlertType.CONFIRMATION);
        porazka.setTitle("PORAZKA!");
        porazka.setHeaderText("Zbankrutowales!");
        porazka.setContentText("Czy chcesz zagrac ponownie?");

        ButtonType buttonTypeOne = new ButtonType("Tak");
        ButtonType buttonTypeTwo = new ButtonType("Nie");

        porazka.getButtonTypes().setAll(buttonTypeOne, buttonTypeTwo);
        Optional<ButtonType> result = porazka.showAndWait();
        if(result.get() == buttonTypeOne) {
            reset();    //ponowne wlaczenie rozgrywki
        }
        else if(result.get() == buttonTypeTwo) {
            Platform.exit();
            System.exit(0);
        }
    }

    @FXML
    public void pressRollButton() throws IOException {
        String print_money;
        String answerColor = null;
        String signOfEnd = "99";    //znak końca wylosowanych przez serwer liczb
        int isGreen = 0;

        //odczytanie wylosowanej przez serwer liczby
        String answer_number = bf.readLine();
        System.out.println(answer_number);
        if(answer_number.equals(signOfEnd)) {
            System.out.println("Koniec gry.");

            Alert endOfNumbers = new Alert(Alert.AlertType.WARNING);
            endOfNumbers.setTitle("Koniec wylosowanych liczb");
            endOfNumbers.setHeaderText("Wykorzystałeś wszystkie wylosowane przez ruletkę liczby.");
            endOfNumbers.setContentText("Hazard uzależnia - graj w umiarkowanych ilościach.");

            ButtonType confirmExit = new ButtonType("Koniec");
            endOfNumbers.getButtonTypes().setAll(confirmExit);
            endOfNumbers.showAndWait();

            Stage stage = (Stage) btnRoll.getScene().getWindow();
            stage.close();
        }

        money -= betMoney;

        if(money <= 0) {
            defeat();
        }

        txtResult.setText(answer_number);
        int receivedNumber = Integer.valueOf(answer_number);

        if(compareInt_Arrays(receivedNumber, isBlack)) {
            txtResult.setStyle("-fx-background-color: black; -fx-text-fill: white");
            answerColor = "CZARNY";
        }
        if(receivedNumber==isGreen) {
            txtResult.setStyle("-fx-background-color: darkgreen; -fx-text-fill: white");
            answerColor = "ZIELONY";
        }
        if(compareInt_Arrays(receivedNumber, isRed)) {
            txtResult.setStyle("-fx-background-color: red; -fx-text-fill: black");
            answerColor = "CZERWONY";
        }

        if(color.equals(answerColor)) {
            if(answerColor.equals("CZARNY") || answerColor.equals("CZERWONY"))
                money = money + betMoney *2;
            else if(answerColor.equals("ZIELONY"))
                money = money + betMoney *5;
            if(betNumber == receivedNumber)
                money = money + betMoney *3;
            else if (betNumber != -1)
                money = money - betMoney *2;
        }
        print_money = Integer.toString(money);
        txtMon.setText(print_money);
        if (currentUser != null) {
            try {
                Statement stmt = conn1.createStatement();
                ResultSet rs = stmt.executeQuery("UPDATE uzytkownik SET money = " + money + " WHERE username = " + "'" + currentUser + "'");
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }

    @FXML
    public void pressBetButton() throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("bet.fxml"));
        Parent root = loader.load();
        Scene scene = new Scene(root, 1000, 360);
        Stage stage = new Stage();
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.setTitle("Obstawianie");
        BetController betcontroller =
                loader.<BetController>getController();
        stage.setScene(scene);
        btnRoll.setDisable(false);
        stage.setResizable(false);
        stage.show();
        stage.setOnHidden(new EventHandler<WindowEvent>() {
            public void handle(WindowEvent we) {
                color = betcontroller.getBetSelectedColor();
                betMoney = betcontroller.getBetMoney();
                betNumber = betcontroller.getBetSelectedNumber();
                if(betNumber == -1)
                    txtBetType.setText("Obstawiono " + betMoney + " na " + color);
                else
                    txtBetType.setText("Obstawiono " + betMoney + " na " + color + " numer: " + betNumber);
            }
        });
    }

    private void reset() {
        String printMoney;
        money = 1000;
        betNumber = NULL;
        betMoney = 0;
        txtResult.setText(null);
        txtBetType.setText(null);
        btnRoll.setDisable(true);
        currentUser = null;
        printMoney = Integer.toString(money);
        txtMon.setText(printMoney);
        txtUsername.setVisible(true);
        txtPassword.setVisible(true);
        btnLogout.setVisible(false);
        btnLogin.setVisible(true);
        btnRegister.setVisible(true);
        btnAccount.setVisible(false);
        txtLoggedIn.setText("TRYB DEMO");
    }

    @FXML
    public void pressLoginButton() throws IOException {
        String typedUsername = txtUsername.getText();
        String typedPassword = txtPassword.getText();
        try {
            Class.forName("oracle.jdbc.OracleDriver");
            String dbURL1 = "jdbc:oracle:thin:" + DB_USERNAME + "/" + DB_PASSWORD + "@localhost:1521:xe";
            conn1 = DriverManager.getConnection(dbURL1);
            if (conn1 != null) {
                System.out.println("Connected with database");
                Statement stmt = conn1.createStatement();
                String printMoney;
                ResultSet rs = stmt.executeQuery("SELECT username as nazwa, password as haslo, money as stan_konta FROM uzytkownik");
                while (rs.next())
                    if (rs.getString(1).equals(typedUsername) && rs.getString(2).equals(typedPassword)) {
                        currentUser = rs.getString(1);
                        txtUsername.setVisible(false);
                        txtPassword.setVisible(false);
                        btnLogout.setVisible(true);
                        btnLogin.setVisible(false);
                        btnRegister.setVisible(false);
                        txtLoggedIn.setText("Zalogowany: " + currentUser);
                        txtPassword.setText(null);
                        btnAccount.setVisible(true);
                        money = rs.getInt(3);
                        printMoney = Integer.toString(money);
                        txtMon.setText(printMoney);
                    }
                if (currentUser == null) {
                    Alert loginFailed = new Alert(Alert.AlertType.WARNING);
                    loginFailed.setTitle("Logowanie nieudane");
                    loginFailed.setHeaderText("Niepoprawna nazwa uzytkownika lub haslo.");
                    loginFailed.setContentText("Nie posiadasz konta? Mozesz je zalozyc wybierajac odpowiednia opcje!");
                    loginFailed.showAndWait();
                }

            }
        }
        catch (ClassNotFoundException ex) {
            ex.printStackTrace();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    @FXML
    public void pressLogoutButton() throws IOException {
            try {
                if (conn1 != null && !conn1.isClosed()) {
                    conn1.close();
                }
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        reset();
    }

    @FXML
    public void pressRegisterButton() throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("register.fxml"));
        Parent root = loader.load();
        Scene scene = new Scene(root, 400, 500);
        Stage stage = new Stage();
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.setResizable(false);
        stage.setTitle("Rejestracja");
        RegisterController registercontroller =
                loader.<RegisterController>getController();
        stage.setScene(scene);
        stage.setResizable(false);
        stage.show();

    }

    @FXML
    public void pressAccountButton() throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("account.fxml"));
        Parent root = loader.load();
        Scene scene = new Scene(root, 400, 400);
        Stage stage = new Stage();
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.setTitle("Konto");
        stage.setResizable(false);
        AccountController accountcontroller =
                loader.<AccountController>getController();
        stage.setScene(scene);
        stage.setResizable(false);
        stage.show();
        accountcontroller.setAccUsername(currentUser);
        stage.setOnHidden(new EventHandler<WindowEvent>() {
            public void handle(WindowEvent we) {
                money = money+accountcontroller.getAccTotalMoneyAmount();
                String printMoney = Integer.toString(money);
                txtMon.setText(printMoney);
            }
        });
    }
}
