package sample;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.Alert;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.text.Text;

import java.io.IOException;
import java.sql.*;

public class AccountController {

    @FXML
    public Text txtAccUsername;

    @FXML
    public TextField txtAccAmount;

    @FXML
    public TextField txtAccOldPassword;

    @FXML
    public TextField txtAccPassword;

    @FXML
    public TextField txtAccConfirm;

    @FXML
    public ToggleGroup tglAccPaymentMethod;

    String accUsername;
    int accMoneyAmount;
    public int accTotalMoneyAmount = 0;

    FXMLLoader loader = new FXMLLoader(getClass().getResource("client.fxml"));
    Parent root = loader.load();
    ClientController clientController =
            loader.<ClientController>getController();
    Connection conn1 = null;

    public AccountController() throws IOException {
    }

    public void setAccUsername(String accUsername)  {
        this.accUsername = accUsername;
        txtAccUsername.setText("Panel uzytkownika " + accUsername);
    }

    @FXML
    public void pressChangePasswordButton() {
        if (txtAccPassword.getText().equals(txtAccConfirm.getText()) && txtAccPassword.getText().length()>4) {
            try {
                Class.forName("oracle.jdbc.OracleDriver");
                String dbURL1 = "jdbc:oracle:thin:" + clientController.DB_USERNAME + "/" + clientController.DB_PASSWORD + "@localhost:1521:xe";
                Connection conn1 = DriverManager.getConnection(dbURL1);
                if (conn1 != null) {
                    Statement stmt = conn1.createStatement();
                    ResultSet securityCheck = stmt.executeQuery("SELECT password FROM uzytkownik WHERE username = '" + accUsername + "'");
                    while (securityCheck.next()) {
                        if (!securityCheck.getString("password").equals(txtAccOldPassword.getText())) {
                            Alert wrongOldPassword = new Alert(Alert.AlertType.ERROR);
                            wrongOldPassword.setTitle("Zmiana hasla nieudana");
                            wrongOldPassword.setHeaderText("Stare haslo sie nie zgadza!");
                            wrongOldPassword.showAndWait();
                            return;
                        }
                    }
                    Statement stmt2 = conn1.createStatement();
                    ResultSet rs2 = stmt2.executeQuery("UPDATE uzytkownik SET password='" + txtAccPassword.getText() + "' WHERE username = '" + accUsername + "'");
                    txtAccOldPassword.setText(null);
                    txtAccPassword.setText(null);
                    txtAccConfirm.setText(null);
                    Alert success = new Alert(Alert.AlertType.INFORMATION);
                    success.setTitle("Gratulacje!");
                    success.setHeaderText("Zmiana hasla udana!");
                    success.showAndWait();
                }
            } catch (SQLException | ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
        else {
            Alert passwordsNotEqual = new Alert(Alert.AlertType.ERROR);
            passwordsNotEqual.setTitle("Zmiana hasla nieudana");
            passwordsNotEqual.setHeaderText("Hasla nie sa takie same lub haslo jest za krotkie!");
            passwordsNotEqual.showAndWait();
        }
    }

    @FXML
    public void pressAddFundsButton() {
        Connection conn1 = null;

        if(!txtAccAmount.getText().matches("\\d+"))
            txtAccAmount.setText("0");

        if (Integer.parseInt(txtAccAmount.getText()) > 9 && Integer.parseInt(txtAccAmount.getText()) < 100000 && tglAccPaymentMethod.getSelectedToggle() != null) {
            try {
                Class.forName("oracle.jdbc.OracleDriver");
                String dbURL1 = "jdbc:oracle:thin:" + clientController.DB_USERNAME + "/" + clientController.DB_PASSWORD + "@localhost:1521:xe";
                conn1 = DriverManager.getConnection(dbURL1);
                if (conn1 != null) {
                    Statement stmt = conn1.createStatement();
                    Statement stmt2 = conn1.createStatement();
                    accMoneyAmount = Integer.parseInt(txtAccAmount.getText());
                    RadioButton selectedRadioButton = (RadioButton) tglAccPaymentMethod.getSelectedToggle();
                    String method = selectedRadioButton.getText();
                    ResultSet rs = stmt.executeQuery("UPDATE uzytkownik SET money=money+" + accMoneyAmount + " WHERE username = '" + accUsername + "'");
                    ResultSet rs2 = stmt2.executeQuery("INSERT INTO transakcja VALUES (transakcja_id_seq.NEXTVAL, SYSDATE," + accMoneyAmount + ",'" + method + "','" + accUsername + "')");
                    accTotalMoneyAmount = accTotalMoneyAmount + accMoneyAmount;
                    txtAccAmount.setText(null);
                    Alert success = new Alert(Alert.AlertType.INFORMATION);
                    success.setTitle("Gratulacje!");
                    success.setHeaderText("Doladowanie srodkow udane!");
                    success.showAndWait();
                }
            } catch (ClassNotFoundException ex) {
                ex.printStackTrace();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
        else {
            Alert monerror = new Alert(Alert.AlertType.ERROR);
            monerror.setTitle("Doladowanie srodkow nieudane");
            monerror.setHeaderText("Kwota musi byc pomiedzy 10 a 100000 PLN oraz metoda platnosci musi byc wybrana.");
            monerror.showAndWait();
        }
    }

    public int getAccTotalMoneyAmount() {
        return accTotalMoneyAmount;
    }

}
